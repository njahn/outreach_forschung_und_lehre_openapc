Neue Marktkonzentration?

Eine Analyse der Open-Access-Kosten in Deutschland | Najko Jahn | Marco Tullney

In den vergangenen Monaten kündigten einzelne Universitäten den Bezug wissenschaftlicher Zeitschriften großer Wissenschaftsverlage wegen zu stark gestiegener Preise. Open Access wurde als günstige Alternative beschrieben, die die Marktmacht der großen Wissenschaftsverlage durchbrechen sollte. Eine aktuelle Studie in Deutschland zu den Open Access-Publikationsgebühren bringt ernüchternde Ergebnisse.

Die Preisentwicklung wissenschaftlicher Journale wird seit vielen Jahren als problematisch erachtet. Laut den Empfehlungen „Zur Zukunft des wissenschaftlichen Publikationssystems“ der Berlin-Brandenburgischen Akademie der Wissenschaften (BBAW) betrifft dies nicht nur Zeitschriftenabos. Vielmehr weite sich das Problem auch auf solche Open-Access-Zeitschriften aus, die eine Publikationsgebühr, häufig auch article-processing charges (APC) genannt, erheben.

In Deutschland fördert die DFG mit ihrem Programm „Open Access Publizieren“ den Aufbau von Publikationsfonds an Universitäten, die zentral für ihre Autorinnen und Autoren solche Publikationskosten übernehmen. Auch außeruniversitäre Forschungseinrichtungen intensivieren entsprechend ihre Förderaktivitäten. Um die Finanzierung kostenpflichtiger Open Access-Artikel transparent darzustellen, dokumentieren seit 2014 wissenschaftliche Einrichtungen ihre Ausgaben als Open Data und machen diese über die Open APC Initiative verfügbar (siehe Infokasten unten). Dadurch kann die institutionelle Förderung von Open Access-Publikationsgebühren nun auf Basis der tatsächlichen Zahlungen an wissenschaftliche Verlage empirisch untersucht werden.

Studie

In welchem Umfang finanzieren wissenschaftliche Einrichtungen Veröffentlichungen ihrer Mitglieder in Open Access-Zeitschriften, die eine Publikationsgebühr erheben? Auf welche Verlage und wissenschaftliche Zeitschriften verteilt sich die Förderung in Deutschland? Um diese Fragen zu beantworten, haben wir in einer Studie (PeerJ, DOI 10.7717/peerj.2323) Ausgaben von Hoch­schulen und außeruniversitären Einrichtungen anhand der Daten der Open APC Initiative von 2005 bis 2015 analysiert. Untersucht wurden neben den institutionellen Ausgaben auch deren Verteilung auf Verlage und Zeitschriften.

Zentrale Ergebnisse

Dreißig Universitäten und außeruniversitäre Forschungseinrichtungen in Deutschland förderten die Veröffentlichung von 7.417 Artikeln im Umfang von 9.627.537 Euro. Die Max-Planck-Gesellschaft, die Publikationsgebühren zentral über die Max Planck Digital Library begleicht, finanzierte rund 39 Prozent aller Veröffentlichungen. Die Ergebnisse bestätigen, dass sich die Förderaktivitäten für Open Access-Zeitschriftenartikel in den letzten Jahren intensiviert haben: So entfiel etwa allein ein Drittel der Ausgaben auf das Jahr 2015 (2.843.857 Euro für 1.999 Artikel).

Im Mittel stiegen die Publikationsgebühren moderat. Die durchschnittliche Gebühr betrug 1.423 Euro im Jahr 2015 und befand sich damit unter der Preisgrenze von 2.000 Euro, die die DFG in ihrem Programm „Open Access Publizieren“ vorgibt. Rund 94 Prozent aller Zahlungen je Artikel blieben in diesem Rahmen. Allerdings zeigte sich in der Analyse, dass die Ausgaben je Artikel zwischen 40 Euro und 7.419 Euro schwankten.

Wie sind diese Ausgaben international einzuordnen? Öffentlich verfügbare Daten britischer Universitäten, des Wellcome Trust und des öster­rei­chi­schen Wissenschaftsfonds FWF bestätigen das Wachstum der institutionellen Förderung von Open Access-Artikeln in den letzten beiden Jahren. Es lässt sich allerdings zeigen, dass Einrichtungen in Deutschland vornehmlich Publikationen in reinen Open-Access-Journalen fördern. Über 99 Prozent der Artikel, die der Open APC Initiative gemeldet wurden, erschienen in solchen Zeitschriften. Im Gegensatz dazu förderten britische Universitäten, Wellcome Trust und FWF mehrheitlich das hybride Open Access. Bei diesem Modell werden einzelne Artikel in Abonnementzeitschriften frei zugänglich gemacht. Die Kosten dafür sind durchschnittlich höher: Im letzten Jahr lag der Durchschnittsbetrag für einen hybriden Zeitschriftenartikel, den die betrachteten europäischen Wissenschaftseinrichtungen finanzierten, über der DFG-Preisgrenze von 2.000 Euro.

Die Ausgabenverteilung auf Verlage und Journale ist schief. Die Finanzierung konzentrierte sich auf zehn Verlage, in deren Journalen rund 92 Prozent aller Artikel erschienen. Rund 30 Prozent der finanzierten Artikel veröffentlichte allein der Verlag Springer Nature. Danach folgen insbesondere naturwissenschaftlich-orientierte Verlage (siehe Abbildung). Bezogen auf Zeitschriftentitel illustriert die Studie die Bedeutung multidisziplinärer Großjournale. So erschien jeder fünfte Artikel in PLOS ONE.
Institutionelle Aufwendungen in Deutschland für Open Access-Publikationsgebühren 2011 bis 2015. Verlagsangaben wurden je Artikel auf Basis der DOI über den Dienst Crossref bezogen. Datenquelle: Open APC Initiative (Stand 13. Mai 2016).

Institutionelle Aufwendungen in Deutschland für Open Access-Publikationsgebühren 2011 bis 2015. Verlagsangaben wurden je Artikel auf Basis der DOI über den Dienst Crossref bezogen. Datenquelle: Open APC Initiative (Stand 13. Mai 2016).

Kontext

Institutionelle Ausgaben, die die Open APC Initiative sammelt, zeigen nur einen Ausschnitt der Zahlungen für Open Access-Zeitschriften. Weder Rabatte, die manche Verlage anbieten, noch eine mögliche Teilung der Kosten zwischen Autorinnen und Autoren und der Einrichtung. Diese Modalitäten könnten für die starken Preisschwankungen verantwortlich sein.

Es muss zudem bei der Interpretation der Ergebnisse beachtet werden, dass die Teilnahme an der Open APC Initiative freiwillig ist und die Studie daher nur die Ausgaben von dreißig Einrichtungen untersuchen konnte. Erfahrungen zeigen außerdem, dass nicht alle Publikationskosten aus zentralen Budgets beglichen werden. Offene Fragen beziehen sich daher auf die Erfassung und den Umfang dieser verteilten Zahlungen. Publizieren wissenschaftliche Autorinnen und Autoren an deutschen Einrichtungen im Sinne der Richtlinien des DFG-Förderprogramms hauptsächlich in reinen Open Access-Zeitschriften? Oder nutzen sie für Publikationskosten in hybriden Zeitschriften andere Budgets, die nicht zentral bewirtschaftet werden?

Die Ergebnisse deuten darauf hin, dass wenige Verlage den Markt für Open Access-Zeitschriften prägen. Hier setzen momentan Bibliotheken an, indem sie pauschale Angebote großer Verlage annehmen, mit denen sich Einreichungs- und Zahlungsprozesse für ihre Autorinnen und Autoren vereinfachen lassen. Verlage bieten zudem Open Access-Optionen für Zeitschriften als Teil der Subskribierung an. Ein solches Open Access-Abkommen, das mehr als 1.600 Journale umfasst, hat im letzten Jahr die Max-Planck-Gesellschaft für ihre wissenschaftlichen Angehörigen mit dem Verlag Springer geschlossen. Derzeit verhandelt die Hochschulrektorenkonferenz für deutsche Wissenschaftseinrichtungen über eine Open Access-Option mit dem Verlag Elsevier.

Übrigens finanzieren Bibliotheken Open-Access-Journale nicht nur über Publikationsgebühren. Zu den internationalen Ansätzen, die über ein solches Finanzierungsmodell hinausgehen und derzeit intensiv diskutiert werden, zählen die Open Library of Humanities oder das Konsortium für Journale der Hochenergiephysik, SCOAP³.

Ob und inwieweit sich die Preise für Open Access-Publikationsgebühren moderat und transparent weiterentwickeln, bleibt abzuwarten. Umso wichtiger erscheint es, den BBAW-Empfehlungen zu folgen, welche ergänzend zum APC-Modell die Erprobung alternativer Wege zur Finanzierung von Open Access-Zeitschriften anregen.

Open APC Initiative

Die Open APC Initiative bietet wissenschaftlichen Einrichtungen seit 2014 einen öffentlich verfügbaren Datenservice zur standardisierten Meldung von Open-Access-Publikationsgebühren. Der Service ist an der UB Bielefeld angesiedelt und wird durch die DINI-AG Elektronisches Publizieren unterstützt. Die DFG fördert das Vorhaben im Rahmen der Ausschreibung „Open Access Transformation“ bis 2018. http://www.intact-project.org/openapc/
 
A U T O R E N

Najko Jahn ist Referent an der Universitätsbibliothek Bielefeld. Ab November 2016 arbeitet er als Data Analyst an der Niedersächsischen Staats- und Universitätsbibliothek Göttingen.

Marco Tullney ist Sozialwissenschaftler. Er leitet den Bereich Publikationsdienste der Technischen Informationsbibliothek (TIB) in Hannover und koordiniert deren Open-Access-Aktivitäten.
